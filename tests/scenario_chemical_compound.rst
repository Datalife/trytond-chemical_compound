==========================
Chemical Compound Scenario
==========================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules


Install checklist::

    >>> config = activate_modules('chemical_compound')


Get Models::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Composition = Model.get('product.composition')
    >>> Ingredient = Model.get('product.composition.ingredient')

Create product::

    >>> unit, = ProductUom.find([('name', '=', "Unit")])

    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('10')
    >>> template.save()
    >>> product, = template.products


Create ingredients::

    >>> ingredient = Ingredient()
    >>> ingredient.name = 'Ingredient'
    >>> ingredient.save()

    >>> active_ingredient = Ingredient()
    >>> active_ingredient.name = 'Active ingredient'
    >>> active_ingredient.active_ingredient = True
    >>> active_ingredient.symbol = 'AI'
    >>> active_ingredient.save()


Create composition::

    >>> composition = Composition()
    >>> composition.name = 'Name'
    >>> ingredient_ = composition.ingredients.new()
    >>> ingredient_.ingredient = ingredient
    >>> ingredient_.percentage = Decimal(40)
    >>> ingredient_ = composition.ingredients.new()
    >>> ingredient_.ingredient = active_ingredient
    >>> ingredient_.percentage = Decimal(60)
    >>> composition.save()
    >>> composition.active_ingredients_names
    '60 % Active ingredient'


Add composition in product::

    >>> product.composition = composition
    >>> product.active_ingredients_names
    '60 % Active ingredient'
    >>> product.save()
